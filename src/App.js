import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
      </div>
    );
  }
}

export default App;

// Starts here!

export const PhotoBox = ({ text, background }) => (
  <div className="photobox" style={{ backgroundImage: `url(${background})` }}>
    <div className="overlay">
      <h2>
        {text}
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </h2>
    </div>
  </div>
);

export const PhotoBoxDemo = ({ text, background }) => (
  <div
    className="photobox-demo"
    style={{ backgroundImage: `url(${background})` }}
  >
    <div className="overlay-demo">
      <h2>
        {text}
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </h2>
    </div>
  </div>
);
