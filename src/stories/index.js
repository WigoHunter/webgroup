import React from 'react';
import { storiesOf, action, linkTo } from '@kadira/storybook';
import Button from './Button';
import Welcome from './Welcome';
import { PhotoBox, PhotoBoxDemo } from '../App.js';

storiesOf('Welcome', module)
  .add('to Storybook', () => (
    <Welcome showApp={linkTo('Button')}/>
  ));

storiesOf('Button', module)
  .add('with text', () => (
    <Button onClick={action('clicked')}>Hello Button</Button>
  ))
  .add('with some emoji', () => (
    <Button onClick={action('clicked')}>😀 😎 👍 💯</Button>
  ));

storiesOf('WebGroup', module)
  .add('example1', () => (
    <PhotoBox
      text="Hello World"
      background="http://www.w3schools.com/css/trolltunga.jpg"
    />
  ))
  .add('reverse engineer it', () => (
    <PhotoBoxDemo
      text="Hello World"
      background="http://www.w3schools.com/css/trolltunga.jpg"
    />
  ));
